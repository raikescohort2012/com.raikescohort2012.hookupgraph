#!/usr/bin/env python3
#encoding: utf-8
"""
TextFile.py

A base class for working with text files.

Dependency of HookupGraphMaker.py
"""

class TextFile:

	def __init__(self, path):
		self.path = path

	def read(self):
		self.handle = open(self.path, 'r')
		self.text = self.handle.read()
		self.handle.close()

		return self.text

	def write(self):
		self.handle = open(self.path, 'w')
		self.handle.write(self.text)
		self.handle.close()