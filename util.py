#!/usr/bin/env python3
#encoding: utf-8
"""
util.py

Utility methods

Dependency of HookupGraphMaker.py
"""

# sort None before numbers
def sort_none_num(num):
	if num is None:
		return True
	return num

def sort_last_name(name):
	names = name.rsplit(' ', 1)

	return (names[1], names[0])