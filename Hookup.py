#!/usr/bin/env python3
#encoding: utf-8
"""
Hookup.py

Representation of a "hookup."

Dependency of HookupGraphMaker.py
"""


class Hookup:

	CURRENT = 0
	OFFICIAL = 1
	HITCHED = 2
	ALCOHOL = 3
	
	def __init__(self, partners, statuses, year):
		self.partners = partners # tuple
		self.statuses = statuses # list of enumerations
		self.year = year		 # number or None