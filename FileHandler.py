#!/usr/bin/env python3
#encoding: utf-8
"""
FileHandler.py

Dependency of HookupGraphMaker.py
"""

import sys
import os
import re

from Hookup import Hookup

class FileHandler:
	
	hookups = []
	
	def __init__(self, wikiPath, graphPath):
		self.wikiPath = wikiPath
		self.graphPath = graphPath
		
		self.parse_wiki()
		self.write_graph()
		
	def parse_wiki(self):
		wikiFile = open(self.wikiPath, 'r')
		self.wikiText = wikiFile.read()
		wikiFile.close()
	
		hookupLines = re.findall(r"^\*\[\[.*?\]\]\*? and \[\[.*?\]\]\*?.*?$", self.wikiText, re.MULTILINE)
		for hookupLine in hookupLines:
			partners = [partner.strip("[]") for partner in re.findall(r"\[\[.*?\]\]", hookupLine)]
		
		
			current = False
			official = False
			hitched = False
			alcohol = False
			if "(dating)" in hookupLine:
				current = True
				official = True
			if "(current)" in hookupLine:
				current = True
			if "(dated)" in hookupLine:
				official = True
			if "]]*" in hookupLine:
				alcohol = True
			if ("(married)" in hookupLine) or ("(engaged)" in hookupLine):
				hitched = True
				
			self.hookups.append(Hookup(partners, current, official, hitched, alcohol))


	def write_graph(self):
	
		graphText = "Graph G {\n"
	
		for hookup in hookups:
			graphText += create_graph_line(hookup)
			
		graphText += "}"
		graphFile.write(self.graphText)
		graphFile.close()
	
	def create_graph_line(self, hookup):
		graphLine = '"{}" -- "{}"'.format(hookup.partners[0], hookup.partners[1])
		
		if hookup.current:
			graphLine = self.add_attribute(graphLine, 'color="red"')
		elif hookup.alcohol:
			graphLine = self.add_attribute(graphLine, 'color="green"')
		if not hookup.official:
			graphLine = self.add_attribute(graphLine, 'style=dashed')
		if hookup.hitched:
			graphLine = self.add_attribute(graphLine, 'style="bold"')
		graphLine += ";\n"
		
		return graphLine
		
	def add_attribute(self, graphLine, attribute):
		if not "]" in graphLine:
			return graphLine + " [{}]".format(attribute)
		
		graphLineHalves = graphLine.split("]",1)
		return "{}, {}]{}".format(graphLineHalves[0], attribute, graphLineHalves[1])
			
			
			
			
			