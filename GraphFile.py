#!/usr/bin/env python3
#encoding: utf-8
"""
GraphFile.py

A class for writing to the GraphViz graph markup file.

Dependency of HookupGraphMaker.py
"""

import sys
from collections import namedtuple

import util
from TextFile import *
from Hookup import *

class GraphFile(TextFile):

	hookups = []

	ATTRIBUTES = {
					Hookup.CURRENT  : 'color="red"',\
					Hookup.OFFICIAL : 'style="solid"',\
					Hookup.HITCHED	: 'style="bold"',\
					Hookup.ALCOHOL : 'color="green"'\
	}

	NUM_RECENT_YEARS = 4

	def __init__(self, path, hookups=[]):
		super().__init__(path)
		self.hookups = hookups

	def write(self):

		# lines are dashed unless "official"
		self.text = 'Graph G {\nedge [style="dashed"];\n'
	
		self.text += self.create_nodes()
		
		for hookup in self.hookups:
			self.text += self.create_edge(hookup)
		
		self.text += "}"
		super().write()

	def create_nodes(self):
		Person = namedtuple('Person', ['name','year'])
		persons = []
		years = []

		for hookup in self.hookups:
			persons += [Person(partner, hookup.year) for partner in hookup.partners]
			years.append(hookup.year)


		# unique
		persons = list(set( persons ))
		years = list(set( years ))

		# first sort by name 
		persons.sort(key=lambda person: util.sort_last_name(person.name))
		# then sort by year, reverse
		persons.sort(reverse=True, key=lambda person: util.sort_none_num(person.year) )
		# result is alphabetical groups of names by year, recent to old
		
		years.sort(key=util.sort_none_num)

		# last 4 years
		self.recentYears = years[-self.NUM_RECENT_YEARS:]

		nodes = ''
		for person in persons:
			nodes += self.create_node(person)
		return nodes
			

	def create_node(self, person):
		line = '"{}"'.format(person.name)
		if person.year in self.recentYears:
			line = self.add_attribute(line, 'style="radial"')
			line = self.add_attribute(line, 'fillcolor="pink"')
			line = self.add_attribute(line, 'color="black"')


		line += ";\n"

		return line


	# create a branch markup line
	def create_edge(self, hookup):
		line = '"{}" -- "{}"'.format(hookup.partners[0], hookup.partners[1])

		for status in hookup.statuses:
			try:
				line = self.add_attribute(line, self.ATTRIBUTES[status])
			except KeyError:
				# hookup has attribute that we don't know what to do with. oh well
				pass

		line += ";\n"
		
		return line
		
	# add GraphViz attribute to the branch (e.g. [style="dashed"])
	def add_attribute(self, line, attribute):
		if "]" not in line:
			return line + " [{}]".format(attribute)
		
		lineHalves = line.split("]",1)
		return "{}, {}]{}".format(lineHalves[0], attribute, lineHalves[1])

