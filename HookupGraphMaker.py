#!/usr/bin/env python3
#encoding: utf-8
"""
HookupGraphMaker.py
Version 1.1.2, 2013-03-05.

A simple script to create a graph image of the JDEK "Hookups" page.

Created by Brendan C Smith on 2012-11-23.
Released into the public domain.
"""

import sys
import os
import getopt

from datetime import datetime

from WikiFile import *
from GraphFile import *

# add project libraries to module lookup list
scriptDir = os.path.dirname(os.path.realpath(__file__))
libPath = os.path.join(scriptDir, "lib")
for library in os.listdir(libPath):
	library = os.path.join(libPath, library)
	if os.path.isdir(library):
		sys.path.append(library)

from sh import which


help_message = '''
This is a simple script to create a graph image of the JDEK "Hookups" page.

-w,--wiki=  : path to existing wiki markup file
-g,--graph= : path to create graph markup file, optional
-i,--image=	: path to create graph image file, optional

Save the markup text of the Hookups page to a file such as wikiMarkup.txt, then use this script as:
python3 HookupGraphMaker.py -w wikiMarkup.txt -g Hookups.txt -i Hookups.png

GraphViz and its command line tools must be installed.
Tested on OS X Mountain Lion. No guarantees that this will work for you. 
'''

class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg


def main(argv=None):

	wikiPath = None
	graphPath = None
	imagePath = None
	
	# parse options
	if argv is None:
		argv = sys.argv
	try:
		try:
			opts, args = getopt.getopt(argv[1:], "hw:g:i:", ["help", "wiki=", "graph=", "image="])
		except (getopt.error, msg):
			raise Usage(msg)
	
		# option processing
		wikiDir = None
		for option, value in opts:
			#if option in ("-v", "--verbose"):
			#	verbose = True
			if option in ("-h", "--help"):
				raise Usage(help_message)
			if option in ("-w", "--wiki"):
				wikiPath = value
				wikiDir = os.path.dirname(wikiPath)
			if option in ("-g", "--graph"):
				graphPath = value
			if option in ("-i", "--image"):
				imagePath = value
				

		if graphPath == None:
			timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
			graphPath =  os.path.join( wikiDir, "Hookup Graph {}.txt".format(timestamp) )
		if imagePath == None:
			imagePath = os.path.splitext(graphPath)[0] + ".png"
	
	# you done fucked up
	except (Usage, err):
		print >> sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
		print >> sys.stderr, "\t for help use --help"
		return 2

	# create a new instance of the graph maker
	HookupGraphMaker(wikiPath, graphPath, imagePath)

class HookupGraphMaker:

	def __init__(self, wikiPath, graphPath, imagePath):
		self.wikiPath = wikiPath
		self.graphPath = graphPath
		self.imagePath = imagePath

		wikiFile = WikiFile(wikiPath)
		graphFile = GraphFile(graphPath, wikiFile.hookups)
		graphFile.write()

		neatoPath = which("neato")
		os.popen("{} -o '{}' -Gstart=rand -Goverlap=false -Tpng '{}'".format(neatoPath, imagePath, graphPath))

if __name__ == "__main__":
	sys.exit(main())