#!/usr/bin/env python3
#encoding: utf-8
"""
WikiFile.py

A class for reading from the hookups wiki markup file.

Dependency of HookupGraphMaker.py
"""

import re

from TextFile import *
from Hookup import *

class WikiFile(TextFile):

	hookups = []

	YEAR_PATTERN = r'^===(?:(?:\d{4}-\d{4})|(?:Long Ago))===$'
	PARTNER_PATTERN = r"\[\[.*?\]\]"
	HOOKUP_PATTERN = r"^\*{0}\*? and {0}\*?.*?$".format(PARTNER_PATTERN)

	KEYWORDS = {
				"(dating)"  : (Hookup.CURRENT, Hookup.OFFICIAL),\
				"(current)" : (Hookup.CURRENT,),\
				"(dated)"	: (Hookup.OFFICIAL,),\
				"(engaged)"	: (Hookup.HITCHED,),\
				"(married)"	: (Hookup.HITCHED,),\
				"]]*"		: (Hookup.ALCOHOL,)\
	}

	def __init__(self, path):
		super().__init__(path)
		self.read()

		
	# find all hookup lines in text, add them
	def read(self):
		super().read()

		yearSplit = re.split(r'({})'.format(self.YEAR_PATTERN), self.text, 0, re.MULTILINE)

		# remove top of page
		yearSplit = yearSplit[1:]

		# remove bottom of page
		yearSplit[-1] = re.split('==.*?==',yearSplit[-1])[0]

		# group hookups
		for i in range(0, len(yearSplit), 2):
			year = yearSplit[i]
			year = year.strip('=')
			try:
				year = int(year.split('-')[0])
			# year is not a number, probably "Long Ago"
			except ValueError:
				year = None


			hookupLines = yearSplit[i+1]
			hookupLines = re.findall(self.HOOKUP_PATTERN, hookupLines, re.MULTILINE)
			for hookupLine in hookupLines:
				self.add_hookup(hookupLine, year)

	#given a line of text, parse and add a hookup object
	def add_hookup(self, line, year):
		partners = [partner.strip("[]") for partner in re.findall(self.PARTNER_PATTERN, line)]

		statuses = []
		# tags
		for keyword in self.KEYWORDS:
			if keyword in line:
				for status in self.KEYWORDS[keyword]:
					statuses.append(status)

		self.hookups.append(Hookup(partners, statuses, year))